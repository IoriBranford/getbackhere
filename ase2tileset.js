let fs = require('fs');
let path = require('path');

process.argv.slice(2).map(asefile=>{
	fs.readFile(asefile, (err, asejson) => {
		if (err) throw err;

		let aseprite = JSON.parse(asejson);
		let name = path.basename(asefile, '.jsonase');
		let tsx = path.join(path.dirname(asefile), name+'.json');

		fs.readFile(tsx, (err, tsxjson)=>{
			let frames = aseprite.frames;
			let meta = aseprite.meta;

			let tileset = !err && JSON.parse(tsxjson) || {
				version: 1.2,
				tiledversion: "1.2.4",
				type: 'tileset',
				tileoffset: { x: 0, y: 0 },
			};

			tileset.name = name;
			tileset.image = meta.image;
			tileset.imagewidth = meta.size.w;
			tileset.imageheight = meta.size.h;
			tileset.tilecount = frames.length;
			tileset.tilewidth = frames[0].frame.w;
			tileset.tileheight = frames[0].frame.h;

			let tiles = [];
			meta.frameTags.map(tag=>{
				let animation = []
				for (i = tag.from; i <= tag.to; ++i) {
					animation.push({
						tileid: i,
						duration: frames[i].duration
					});
				}
				tiles.push({
					animation: animation,
					id: tag.from,
					type: tag.name
				});
			});
			tileset.tiles = tiles;

			fs.writeFile(tsx, JSON.stringify(tileset), (err) => {
				if (err) throw err;
			});
		});
	});
});

