let startGame = (...mapFiles)=>{
	let {
		init, initPointer, onPointerDown,
		load, loadData, dataAssets, audioAssets,
		TileEngine,
		GameLoop,
		keyPressed
	} = kontra;

	let { TileSpriteSheets, SpritePool } = sprites;

	let { min, floor, random } = Math;

	let { canvas } = init();
	let tileEngines = {};
	let spriteSheets = {};
	let spritePool;

	let scrollSpeed;

	let prisoner;
	const prisonerRunSpeed = 3;

	let guardSpawnTimer;
	let guardLevel;
	const guardTypes = {
		guard : {
			spritesheet : 'people1',
			runSpeed : 1,
			dragSpeed : -1,
			tapsToBreakFree : 5,
			breakFreeIncLevel : 1
		},
		lasso : {
			spritesheet : 'people2',
			runSpeed : 1,
			dragSpeed : -1,
			tapsToBreakFree : 10,
			breakFreeIncLevel : 0
		}
	};
	const guardTypesByLevel = [ 'guard', 'lasso' ];

	const guardSpawnInterval = 240;
	const guardFallBackSpeed = -2;
	let guardShouts = [ 'hey0.mp3', 'hey1.mp3' ];
	let guardGetBackHeres = [ 'gbh0.mp3', 'gbh1.mp3' ];
	let guardGrunts = [ 'ow0.mp3', 'ow1.mp3' ];

	let currentUpdate;
	let updateStart, updateGame, updateEnd;

	let playRandomSound = (sounds)=>{
		console.log(sounds);
		console.log(audioAssets);
		let sound = sounds[floor(random()*sounds.length)];
		sound.play();
	};

	let initGame = ()=>{
		currentUpdate = updateStart;
		scrollSpeed = 0;
		guardLevel = 0;
		guardSpawnTimer = 0;

		prisoner = spritePool.get({
			spritesheet : 'people1',
			x : -16,
			y : 112,
			guardsPulling : [],
			tapsToBreakFree : 0
		});
		prisoner.playAnimation('prisoner_run');

		onPointerDown((e, obj)=>{
			if (!prisoner)
				return;
			let taps = prisoner.tapsToBreakFree;
			if (taps > 0) {
				++prisoner.x;
				prisoner.guardsPulling.map(guard=>{
					++guard.x;
				});
				if (--taps <= 0) {
					prisoner.playAnimation('prisoner_run');
					prisoner.dx = prisonerRunSpeed;
					prisoner.guardsPulling.map(guard=>{
						guard.playAnimation(guard.type+'_fall');
						guard.dx = guardFallBackSpeed;
						let type = guardTypes[guard.type];
						guardLevel += type.breakFreeIncLevel;
						guardLevel = min(guardLevel, guardTypesByLevel.length);
					});
					prisoner.guardsPulling.length = 0;
					playRandomSound(guardGrunts);
				}
				prisoner.tapsToBreakFree = taps;
			} else {
				prisoner.dx = prisonerRunSpeed;
			}
		});

		return load(...guardShouts)
			.then(()=>load(...guardGetBackHeres))
			.then(()=>load(...guardGrunts)).then(()=>{
			guardShouts = guardShouts.map(file=>audioAssets[file]);
			guardGetBackHeres = guardGetBackHeres.map(file=>audioAssets[file]);
			guardGrunts = guardGrunts.map(file=>audioAssets[file]);
		});
	}

	let atCenter = ()=>(prisoner.x + prisoner.dx >= canvas.width/2);
	let atEnd = ()=>(tileEngines['prison.json'].sx + canvas.width >= tileEngines['prison.json'].tilewidth * tileEngines['prison.json'].width);

	updateStart = ()=>{
		if (atCenter()) {
			currentUpdate = updateGame;
		}
	}

	let addGuard = ()=> {
		let typeName = guardTypesByLevel[floor(random()*guardLevel)];
		let type = guardTypes[typeName];
		let spriteSheet = spriteSheets[type.spritesheet];
		let guard = spritePool.get({
			type : typeName,
			spritesheet : type.spritesheet,
			x : -spriteSheet.framewidth,
			y : prisoner.y,
			dx : type.runSpeed
		});
		let runAnimation = typeName+'_run';
		guard.update = (dt)=>{
			guard.advance(dt);
			let anims = guard.animations;
			let curr = guard.currentAnimation;
			if (curr == anims[runAnimation]){
				if (guard.collidesWith(prisoner)) {
					scrollSpeed = 0;
					let dragSpeed = type.dragSpeed;
					guard.playAnimation(guard.type+'_pull');
					prisoner.dx = min(prisoner.dx + dragSpeed, dragSpeed);
					prisoner.playAnimation('prisoner_struggle');
					prisoner.tapsToBreakFree += type.tapsToBreakFree;
					prisoner.guardsPulling.push(guard);
					prisoner.guardsPulling.map(guard=>{
						guard.dx = prisoner.dx;
					});
					playRandomSound(guardGetBackHeres);
				}
			}
		};
		guard.playAnimation(runAnimation);
	}

	updateGame = ()=>{
		if (!prisoner) {
			return;
		}

		if (prisoner.tapsToBreakFree > 0) {
			if (prisoner.x <= -prisoner.width) {
				prisoner.ttl = 0;
				prisoner = null;
			}
		} else {
			if (atCenter()) {
				prisoner.dx = 0;
				scrollSpeed = prisonerRunSpeed;
			}

			if (atEnd()) {
				currentUpdate = updateEnd;
			} else if (--guardSpawnTimer <= 0) {
				addGuard();
				playRandomSound(guardShouts);
				guardSpawnTimer += guardSpawnInterval;
			}
		}

		tileEngines['prison.json'].sx += scrollSpeed;
	}

	updateEnd = ()=>{
		prisoner.dx = prisonerRunSpeed;
		scrollSpeed = 0;
	}

	let update = ()=>{
		currentUpdate();
		spritePool.update();
	}

	let render = ()=>{ // render the game state
		tileEngines['prison.json'].render();
		spritePool.render();
	}

	load(...mapFiles).then(tilemaps => {
		let sources = [];
		let assets = [];
		tilemaps.map(tilemap=>{
			tilemap.tilesets.map(tileset => {
				if (tileset.source) {
					sources.push(tileset.source);
				} else if (tileset.image) {
					assets.push(tileset.image);
				}
			});
		});

		return load(...assets).then(() => load(...sources));
	}).then(sources => {
		let images = sources.map(tileset => tileset.image);
		return load(...images);
	}).then(() => {
		mapFiles.map(mapFile=> {
			let tilemap = dataAssets[mapFile];
			let tileEngine = TileEngine(tilemap);
			tileEngines[mapFile] = tileEngine;
			Object.assign(spriteSheets, TileSpriteSheets(tileEngine.tilesets));
		});
		spritePool = SpritePool(spriteSheets);

		initPointer();
		return initGame();
	}).then(()=>{
		let loop = GameLoop({ update: update, render: render });
		loop.start();
	});
}

startGame('prison.json', 'people.json');
