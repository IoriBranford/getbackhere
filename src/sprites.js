var sprites = (function() {
	let { SpriteSheet, Sprite, Pool } = kontra;

	let TileSpriteSheets = (tilesets)=>{
		let spriteSheets = {};
		tilesets.map(tileset => {
			if (!tileset.tiles)
				return;

			let animations = null;
			tileset.tiles.map(tile => {
				let loop = true;
				if (tile.properties) {
					tile.properties = tile.properties.reduce(
						(properties, property)=>{
							properties[property.name] = property.value;
							return properties;
						}, {});
					loop = tile.properties.loop;
				}
				if (tile.animation) {
					animations = animations || {};
					let duration = null;
					let frames = [];
					tile.animation.map(frame => {
						duration = duration || frame.duration;
						frames.push(frame.tileid);
					});
					let name = tile.type || tile.id;
					animations[name] = {
						frames : frames,
						frameRate : 1000/duration,
						loop : loop
					};
				}
			});

			if (!animations)
				return;

			let spritesheet = SpriteSheet({
				image : tileset.image,
				frameWidth : tileset.tilewidth,
				frameHeight : tileset.tileheight,
				animations : animations
			})
			let tileoffset = tileset.tileoffset || { x:0, y:0 };
			spritesheet.anchor = {
				x : -tileoffset.x / tileset.tilewidth,
				y : 1 - (tileoffset.y / tileset.tileheight)
			};
			spriteSheets[tileset.name] = spritesheet;
		});
		return spriteSheets;
	}

	let SpritePool = (spriteSheets) => {
		let pool = Pool({ create : Sprite });
		pool.__get = pool.get;
		pool.get = (params = {}) => {
			let spritesheet = params.spritesheet;
			if (typeof(spritesheet) == "string") {
				spritesheet = spriteSheets[spritesheet];
			}
			if (spritesheet) {
				params.animations = params.animations || spritesheet.animations;
				params.anchor = params.anchor || spritesheet.anchor;
			}
			return pool.__get(params);
		};
		return pool;
	}

	return {
		TileSpriteSheets : TileSpriteSheets,
		SpritePool : SpritePool
	};
}());
