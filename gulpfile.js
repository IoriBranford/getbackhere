const gulp     = require('gulp');
const terser   = require('gulp-terser');
const imagemin = require('gulp-imagemin');
const cleancss = require('gulp-clean-css');
const htmlmin  = require('gulp-htmlmin');
const jsonmin  = require('gulp-jsonminify');

const srcdir = 'src';
const outdir = 'public';

gulp.task('build:html', () => {
	return gulp.src(srcdir+'/*.html')
		.pipe(htmlmin({
			collapseWhitespace : true,
			minifyCSS : true
		}))
		.pipe(gulp.dest(outdir));
});

gulp.task('build:css', () => {
	return gulp.src(srcdir+'/*.css')
		.pipe(cleancss())
		.pipe(gulp.dest(outdir));
});

gulp.task('build:images', () => {
	return gulp.src(srcdir+'/*.png')
		.pipe(imagemin())
		.pipe(gulp.dest(outdir));
});

gulp.task('build:audio', () => {
	return gulp.src(srcdir+'/*.mp3')
		.pipe(gulp.dest(outdir));
});

gulp.task('build:js', () => {
	return gulp.src(srcdir+'/*.js')
		.pipe(terser())
		.pipe(gulp.dest(outdir));
});

gulp.task('build:json', () => {
	return gulp.src(srcdir+'/*.json')
		.pipe(jsonmin())
		.pipe(gulp.dest(outdir));
});

// "gulp build" means build everything
gulp.task('build', gulp.series('build:html', 'build:css', 'build:images', 'build:audio', 'build:js', 'build:json'));

// "gulp watch" watches for changed files and then runs just the approprate step
gulp.task('watch', () => {
	gulp.watch(srcdir+'/*.html', gulp.series('build:html'));
	gulp.watch(srcdir+'/*.css',  gulp.series('build:css'));
	gulp.watch(srcdir+'/*.png',  gulp.series('build:images'));
	gulp.watch(srcdir+'/*.mp3',  gulp.series('build:audio'));
	gulp.watch(srcdir+'/*.js',   gulp.series('build:js'));
	gulp.watch(srcdir+'/*.json', gulp.series('build:json'));
});

// "gulp" builds everything and then starts watching
gulp.task('default', gulp.series('build', 'watch'));
