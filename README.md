# Get Back Here
by Iori Branford
for js13kgames 2019

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

## About
Get out of jail before the guards drag you back to your cell. Avoid lassos, nets, tranquilizer darts, trapdoor pits, security doors, electric fences, and more.

![](https://lh3.googleusercontent.com/tDJM1OwdqmX9-L2xa8FOqQSE9odzrXhg71N_sQZQof0g6eYXil7kgnl9lLGXyWMZPW5T1Dk19MRT "Quick concept")

## Game

### Object
- Exit the prison to win.
- Get dragged off the left edge of the screen to lose.

### Controls
- Autorun down a long 3-story hallway.
- Go up or down 1 story to avoid hazards. Jump and pull yourself up onto a ledge, or drop down a hole in the floor.
- If caught in a hazard, mash keys until you break free.

### Hazards
- Guard behind you comes up to grab you. If he catches you he drags you leftward.
  - Upgrade: Longer grab range using an animal restraining pole (with a lasso on the end).
- Guard ahead of you lets you run into him. First shouts a warning visible on the right edge of the screen.
- Nets catch you if you run into them. If caught you cannot move until you break out.
- Guard appears on the left with a tranquilizer gun and fires. If hit you run slower; mash keys to recover.
- Guard flips a switch to open a trapdoor, either above you to drop a net, or below you to drop you into a more hazardous route.
